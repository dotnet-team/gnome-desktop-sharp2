Source: gnome-desktop-sharp2
Section: cli-mono
Priority: optional
Maintainer: Debian CLI Libraries Team <pkg-cli-libs-team@lists.alioth.debian.org>
Uploaders: Mirco Bauer <meebey@debian.org>,
           Sebastian Dröge <slomo@debian.org>,
           Jo Shields <directhex@apebox.org>
Build-Depends: dh-autoreconf (>= 4),
               cli-common-dev (>= 0.5.4),
               debhelper (>= 7),
               gtk-sharp2-gapi (>= 2.12.7),
               libart2.0-cil-dev,
               libglade2.0-cil-dev,
               libglib2.0-dev (>= 2.12.0),
               libgnome-desktop-dev (>= 2.24.0),
               libgnome-vfs2.0-cil-dev,
               libgnome2.0-cil-dev,
               libgtk2.0-cil-dev,
               libgtk2.0-dev (>= 2.12.2),
               libgtkhtml3.14-dev (>= 3.18.0),
               libgtksourceview2.0-dev (>= 2.2.2),
               librsvg2-dev (>= 2.22.2),
               libtool,
               libvte-dev (>= 1:0.16.14),
               libwnck-dev (>= 2.23.0),
               mono-devel (>= 2.4.3),
               pkg-config
Standards-Version: 3.9.2
Homepage: http://www.mono-project.com/GtkSharp
Vcs-Git: git://git.debian.org/pkg-cli-libs/packages/gnome-desktop-sharp2.git
Vcs-Browser: http://git.debian.org/?p=pkg-cli-libs/packages/gnome-desktop-sharp2.git

Package: gnome-desktop-sharp2
Architecture: all
Depends: libgnomedesktop2.0-cil-dev,
         libgtkhtml3.14-cil-dev,
         libgtksourceview2-cil-dev,
         librsvg2-2.0-cil-dev,
         libvte0.16-cil-dev,
         libwnck1.0-cil-dev,
         ${misc:Depends}
Description: GNOME Desktop# 2.24 suite, CLI bindings for GNOME
 GNOME Desktop# 2.24 is a CLI (.NET) language binding for the GNOME 2.24
 desktop libraries.
 .
 This is a metapackage containing dependencies for the complete
 GNOME Desktop# 2.24 suite.

Package: libgtkhtml3.16-cil
Architecture: any
Suggests: monodoc-gtk2.0-manual
Depends: ${cli:Depends}, ${misc:Depends}, ${shlibs:Depends}
Conflicts: libgnome-cil
Description: CLI binding for GtkHTML 3.24
 This package provides the gtkhtml-sharp assembly that allows CLI (.NET)
 programs to use the GtkHTML library 3.24.
 .
 GNOME Desktop# 2.24 is a CLI (.NET) language binding for the GNOME 2.24
 desktop libraries.
 .
 This package contains the GtkHTML assembly itself

Package: libgtkhtml3.14-cil-dev
Architecture: any
Depends: libgtk2.0-cil-dev,
         libgtkhtml3.16-cil (= ${binary:Version}),
         ${misc:Depends}
Replaces: libgtkhtml3.16-cil (<< 2.26.0-2)
Description: CLI binding for GtkHTML 3.24
 This package provides the gtkhtml-sharp assembly that allows CLI (.NET)
 programs to use the GtkHTML library 3.24.
 .
 GNOME Desktop# 2.24 is a CLI (.NET) language binding for the GNOME 2.24
 desktop libraries.
 .
 This package contains development files for the GtkHTML library, and should
 be used for compilation

Package: librsvg2-2.18-cil
Architecture: all
Suggests: monodoc-gtk2.0-manual
Depends: ${cli:Depends}, ${misc:Depends}, ${shlibs:Depends}
Description: CLI binding for RSVG 2.22
 This package provides the rsvg-sharp assembly that allows CLI (.NET)
 programs to use the RSVG library 2.22.
 .
 GNOME Desktop# 2.24 is a CLI (.NET) language binding for the GNOME 2.24
 desktop libraries.
 .
 This package contains the rsvg-sharp assembly itself

Package: librsvg2-2.0-cil-dev
Architecture: all
Depends: librsvg2-2.18-cil (= ${binary:Version}), ${misc:Depends}
Replaces: librsvg2-cil-dev (<< 2.26.0-2)
Description: CLI binding for RSVG 2.22
 This package provides the rsvg-sharp assembly that allows CLI (.NET)
 programs to use the RSVG library 2.22.
 .
 GNOME Desktop# 2.24 is a CLI (.NET) language binding for the GNOME 2.24
 desktop libraries.
 .
 This package contains development files for the rsvg-sharp library, and should
 be used for compilation

Package: libvte0.16-cil
Architecture: any
Suggests: monodoc-gtk2.0-manual
Depends: ${cli:Depends}, ${misc:Depends}, ${shlibs:Depends}
Description: CLI binding for VTE 0.16
 This package provides the vte-sharp assembly that allows CLI (.NET) programs
 to use the VTE libraries 0.16.
 .
 GNOME Desktop# 2.24 is a CLI (.NET) language binding for the GNOME 2.24
 desktop libraries.
 .
 This package contains the vte-sharp assembly itself

Package: libvte0.16-cil-dev
Architecture: any
Depends: libgtk2.0-cil-dev,
         libvte0.16-cil (= ${binary:Version}),
         ${misc:Depends}
Replaces: libvte0.16-cil (<< 2.26.0-2)
Description: CLI binding for VTE 0.16
 This package provides the vte-sharp assembly that allows CLI (.NET) programs
 to use the VTE libraries 0.16.
 .
 GNOME Desktop# 2.24 is a CLI (.NET) language binding for the GNOME 2.24
 desktop libraries.
 .
 This package contains development files for the vte-sharp library, and should
 be used for compilation

Package: libgnomedesktop2.20-cil
Architecture: all
Suggests: monodoc-gtk2.0-manual
Depends: ${cli:Depends}, ${misc:Depends}, ${shlibs:Depends}
Description: CLI binding for GNOME Desktop 2.24
 This package provides the gnome-desktop-sharp assembly that allows CLI (.NET)
 programs to use the GNOME Desktop libraries 2.24.
 .
 GNOME Desktop# 2.24 is a CLI (.NET) language binding for the GNOME 2.24
 desktop libraries.
 .
 This package contains the gnome-desktop-sharp assembly itself

Package: libgnomedesktop2.0-cil-dev
Architecture: all
Depends: libgnome2.0-cil-dev,
         libgnomedesktop2.20-cil (= ${binary:Version}),
         ${misc:Depends}
Replaces: libgnomedesktop2.20-cil (<< 2.26.0-2)
Description: CLI binding for GNOME Desktop 2.24
 This package provides the gnome-desktop-sharp assembly that allows CLI (.NET)
 programs to use the GNOME Desktop libraries 2.24.
 .
 GNOME Desktop# 2.24 is a CLI (.NET) language binding for the GNOME 2.24
 desktop libraries.
 .
 This package contains development files for the gnome-desktop-sharp library,
 and should be used for compilation

Package: libgtksourceview2-2.0-cil
Architecture: any
Suggests: monodoc-gtk2.0-manual
Depends: ${cli:Depends}, ${misc:Depends}, ${shlibs:Depends}
Description: CLI binding for GtkSourceView 2.2
 This package provides the gtk-sourceview-sharp assembly that allows CLI (.NET)
 programs to use the GtkSourceView libraries 2.2.
 .
 GNOME Desktop# 2.24 is a CLI (.NET) language binding for the GNOME 2.24
 desktop libraries.
 .
 This package contains the gtk-sourceview-sharp assembly itself

Package: libgtksourceview2-cil-dev
Architecture: any
Depends: libgtk2.0-cil-dev,
         libgtksourceview2-2.0-cil (= ${binary:Version}),
         ${misc:Depends}
Replaces: libgtksourceview2-2.0-cil (<< 2.26.0-2)
Description: CLI binding for GtkSourceView 2.2
 This package provides the gtk-sourceview-sharp assembly that allows CLI (.NET)
 programs to use the GtkSourceView libraries 2.2.
 .
 GNOME Desktop# 2.24 is a CLI (.NET) language binding for the GNOME 2.24
 desktop libraries.
 .
 This package contains development files for the gtk-sourceview-sharp library,
 and should be used for compilation

Package: libwnck2.20-cil
Architecture: any
Suggests: monodoc-gtk2.0-manual
Depends: ${cli:Depends}, ${misc:Depends}, ${shlibs:Depends}
Description: CLI binding for wnck 2.24
 This package provides the wnck-sharp assembly that allows CLI (.NET) programs
 to use the wnck libraries 2.24.
 .
 GNOME Desktop# 2.24 is a CLI (.NET) language binding for the GNOME 2.24
 desktop libraries.
 .
 This package contains the wnck-sharp assembly itself

Package: libwnck1.0-cil-dev
Architecture: any
Depends: libgtk2.0-cil-dev,
         libwnck2.20-cil (= ${binary:Version}),
         ${misc:Depends}
Replaces: libwnck2.20-cil (<< 2.26.0-2)
Description: CLI binding for wnck 2.24
 This package provides the wnck-sharp assembly that allows CLI (.NET) programs
 to use the wnck libraries 2.24.
 .
 GNOME Desktop# 2.24 is a CLI (.NET) language binding for the GNOME 2.24
 desktop libraries.
 .
 This package contains development files for the wnck-sharp library, and should
 be used for compilation
